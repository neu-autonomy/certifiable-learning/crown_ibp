from sys import platform

from setuptools import setup

if platform == "linux" or platform == "linux2":
    tensorflow_pkg = "tensorflow-cpu"
elif platform == "darwin":
    tensorflow_pkg = "tensorflow-macos"

setup(
    name="crown_ibp",
    version="1.0.0",
    install_requires=[
        "torch",
        tensorflow_pkg,
    ],
    packages=["crown_ibp", "crown_ibp.conversions"],
)
