#!/usr/bin/env python3

import argparse

import numpy as np
import tensorflow as tf
import torch
import torch.nn as nn

# import torchvision.transforms as transforms
# import torchvision.datasets as datasets
import torch.nn.functional as F
from crown_ibp.conversions.mnist_cifar_models import (
    NLayerModel,
    get_model_meta,
)
from crown_ibp.conversions.setup_mnist import MNIST
from crown_ibp.conversions.utils import show
from PIL import Image
from tensorflow.keras.layers import Activation as KerasActivation
from tensorflow.keras.layers import Activation as TFActivation
from tensorflow.keras.layers import Dense as KerasDense
from tensorflow.keras.layers import Dense as TFDense
from tensorflow.keras.layers import Flatten as KerasFlatten
from tensorflow.keras.layers import Flatten as TFFlatten
from tensorflow.keras.models import Sequential


class Flatten(nn.Module):
    def forward(self, x):
        y = x.view(x.size(0), -1)
        return y


def keras2torch(keras_model, output):
    modules = []

    if isinstance(keras_model, Sequential):
        layers = keras_model.layers
    else:
        layers = keras_model.model.layers

    for l in layers:
        if isinstance(l, TFDense) or isinstance(l, KerasDense):
            try:
                # if old keras version... (TODO: check properly, not a try except)
                input_shape = l.input_shape[1]
                output_shape = l.output_shape[1]
            except:
                # in case of modern keras version... (2024)
                input_shape = l.input.shape[-1]
                output_shape = l.output.shape[-1]
            linear = nn.Linear(input_shape, output_shape)
            w, b = l.get_weights()
            linear.weight.data.copy_(torch.Tensor(w.T.copy()))
            linear.bias.data.copy_(torch.Tensor(b))
            modules.append(linear)

            ### Dense layers might have an activation built in...
            if "relu" in str(l.activation):
                modules.append(nn.ReLU())
            elif "tanh" in str(l.activation):
                modules.append(nn.Tanh())
            elif "sigmoid" in str(l.activation):
                modules.append(nn.Sigmoid())
            elif "linear" in str(l.activation):
                pass
            else:
                print(str(l.activation))
                raise (ValueError("Unsupported activation"))

        elif isinstance(l, TFFlatten) or isinstance(l, KerasFlatten):
            modules.append(Flatten())
        elif isinstance(l, TFActivation) or isinstance(l, KerasActivation):
            if "relu" in str(l.activation):
                modules.append(nn.ReLU())
            elif "tanh" in str(l.activation):
                modules.append(nn.Tanh())
            elif "sigmoid" in str(l.activation):
                modules.append(nn.Sigmoid())
            else:
                raise (ValueError("Unsupported activation"))
        else:
            print(l)
            raise (ValueError("Unknown layer", l))

    torch_model = nn.Sequential(*modules)
    torch.save(torch_model.state_dict(), output)
    return torch_model


def get_keras_model(input):
    weight_dims, activation, activation_param, _ = get_model_meta(input)
    keras_model = NLayerModel(
        weight_dims[:-1],
        input,
        activation=activation,
        activation_param=activation_param,
    )
    return keras_model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="input file", required=True)
    parser.add_argument("-o", "--output", help="output file", required=True)
    args = parser.parse_args()

    keras_model = get_keras_model(args.input)
    torch_model = keras2torch(keras_model, args.output)

    print(torch_model)

    data = MNIST()
    n_data = 500
    pred_data = data.test_data[:n_data]
    pred_label = data.test_labels[:n_data]

    tf_predict = keras_model.model.predict(pred_data)
    torch_predict = torch_model(torch.Tensor(pred_data)).detach().numpy()
    print(
        "prediction difference:",
        np.linalg.norm((tf_predict - torch_predict).flatten(), ord=1) / n_data,
    )
    true_labels = np.argmax(pred_label, axis=1)
    tf_labels = np.argmax(tf_predict, axis=1)
    torch_labels = np.argmax(torch_predict, axis=1)
    print("tensorflow acc:", np.sum(true_labels == tf_labels) / n_data)
    print("pytorch acc:", np.sum(true_labels == torch_labels) / n_data)
